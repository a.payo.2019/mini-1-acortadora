import webapp
from urllib import parse
import random
import string

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>url</label>
        <input type="text" name="url" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>{url}:</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {url}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""



class Randomshort(webapp.webApp):

    urls = {}

    def parse(self, request):
        """Return the method name and resource name"""

        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start+4:]
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        return (data)


    def process(self, data):
        """Produce the page with the content for the resource"""

        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed",\
                         PAGE_NOT_ALLOWED.format(method=data['method'])
        return (code, page)

    def get(self, url):
        if url == '/':
            code = '200 OK'
            page = PAGE.format(form=FORM, url=url)

        else:
            if url in Randomshort.urls:
                realurl = Randomshort.urls[url]
                page = ''
                code = "301 Moved permanently" + 'Location: ' + realurl
            else:
                page = PAGE_NOT_FOUND.format(url=url, form=FORM)
                code = "404 Resource Not Found"
        return code, page

    def post(self, url, body):
        fields = parse.parse_qs(body)
        print("Fields:", fields)
        if (url == '/'):
            if ('url' in fields):
                realurl = fields['url'][0]
                nuevourl = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))
                Randomshort.urls[nuevourl] = realurl
                page = PAGE.format(url=url, form=FORM)
                code = "200 OK"
            else:
                page = PAGE_UNPROCESABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            code = "405 Method not allowed"
            page = PAGE_NOT_ALLOWED.format(method='POST')
        return code, page


if __name__ == "__main__":
    webApp = Randomshort("localhost", 1234)